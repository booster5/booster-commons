# Booster Commons

## Purpose 

Commons library includes some functionality that are used by all booster libraries:
1. Metrics reporting using micrometer.
   1. Provides convenient methods to report metrics
   2. Enables metric reporting for thread pools
2. Retry configuration using resilience4j.
   1. Allows customization of retries.
3. Circuit breaker configuration using resilience4j.

## Getting started

### Project Setup

#### Maven

pom.xml
```xml
<dependency>
  <groupId>io.gitlab.booster</groupId>
  <artifactId>booster-commons</artifactId>
  <version>VERSION_TO_USE</version>
</dependency>
```
if you haven't yet set up your repository:
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/61046646/-/packages/maven</url>
  </repository>
</repositories>
```

#### Gradle Groovy

```groovy
implementation 'io.gitlab.booster:booster-commons:VERSION_TO_USE'
```
setup maven repository:
```groovy
maven {
  url 'https://gitlab.com/api/v4/groups/61046646/-/packages/maven'
}
```

#### Gradle Kotlin

```kotlin
implementation("io.gitlab.booster:booster-commons:VERSION_TO_USE")
```
setup maven repository:
```kotlin
maven("https://gitlab.com/api/v4/groups/61046646/-/packages/maven")
```

## Content

### Internal Object Cache

A ```GenericKeyedObjectCache``` implements ```KeyedObjectCache``` where objects
can be retrieved using a key. If the object corresponding to the key doesn't exist,
the pool can return either a null, or Option of null depending on which method
is used to fetch the object. There are multiple places where such a cache is useful,
and hence the implementation is extracted to provide easier use.

To provide a new implementation, one needs to extend ```GenericKeyedObjectCache```, and
implement ```KeyedCachedObjectFactory``` which is responsible to create a new
object based on the key when no entries are found in cache.

### Circuit Breaker Configuration

[Circuit breaker](https://resilience4j.readme.io/docs/circuitbreaker) configuration along with retries are used mostly on Booster tasks,
but these two are extracted into a common package to be reused by other Booster libraries.

[Circuit breaker](https://resilience4j.readme.io/docs/circuitbreaker) uses [resilience4j](https://resilience4j.readme.io/docs/getting-started).

Refer to [circuit breaker documentation](https://resilience4j.readme.io/docs/circuitbreaker#create-and-configure-a-circuitbreaker) for
a full list of settings available.

### Retries

[Retries](https://resilience4j.readme.io/docs/retry) also uses [resilience4j](https://resilience4j.readme.io/docs/getting-started).

| Option               | Possible Values               | Comment                                                                            |
|----------------------|-------------------------------|------------------------------------------------------------------------------------|
| maxAttempts          | integer values greater than 0 | total number of attempts including the initial one                                 |
| backOffPolicy        | LINEAR or EXPONENTIAL         | whether back off time remains constant or grows exponentially                      |
| initialBackOffMillis | integer values greater than 0 | initial back off time in milliseconds, if less than or equal to 0, defaults to 100 | 

### Micrometer Metrics

Booster uses [micrometer](https://micrometer.io/) to report metrics like many
other Java libraries. ```MetricsRegistry``` provides easy ways to report such
metrics, as well as enabling metrics reporting for certain libraries that are by
default turned off, such as reporting metrics for a thread pool.
