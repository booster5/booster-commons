# Change Log

## v3.2.0
1. Update to Spring Boot 3.4.2
2. Update to Spring Cloud 2024.0.0
3. Update to Spring Cloud GCP 6.0.0
4. Update to JVM 21
5. Update dependencies

## v3.1.0
1. Update to Spring Boot 3.3.7
2. Update to Spring Cloud 2023.0.4
3. Update to Spring Cloud GCP 5.9.0

## v3.0.0
1. Change to Spring Boot 3.2.5

## v2.0.0
1. Change to booster-base-pom v2.0.0

## v1.5.0
1. Use common parent pom
2. Upgrade libraries to latest version.

## v1.0.1
1. Renaming circuit breaker config fields.

## v1.0.0
1. Initial version
