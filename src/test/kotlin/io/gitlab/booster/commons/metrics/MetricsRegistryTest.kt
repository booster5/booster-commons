package io.gitlab.booster.commons.metrics

import arrow.core.Option.Companion.fromNullable
import arrow.core.getOrElse
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.not
import org.hamcrest.Matchers.notNullValue
import org.hamcrest.Matchers.sameInstance
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

internal data class TestData(var data: String)

internal class MetricsRegistryTest {

    private var meterRegistry: MeterRegistry? = null
    @BeforeEach
    fun setup() {
        meterRegistry = SimpleMeterRegistry()
    }

    @Test
    fun shouldBuild() {
        assertThat(meterRegistry, notNullValue())
        assertThat(MetricsRegistry(meterRegistry), notNullValue())
    }

    @Test
    fun shouldCreateTimer1() {
        val registry = MetricsRegistry(meterRegistry)
        val sampleOption = registry.startSample()
        registry.endSample(sampleOption, "test")
        assertThat(sampleOption.isSome(), equalTo(true))
    }

    @Test
    fun shouldCreateTimer2() {
        val registry = MetricsRegistry(meterRegistry)
        val sampleOption = registry.startSample()
        registry.endSample(sampleOption, "test", "tag", "value")
        assertThat(sampleOption.isSome(), equalTo(true))
    }

    @Test
    fun shouldCreateCounter1() {
        val registry = MetricsRegistry(meterRegistry)
        registry.incrementCounter("test1")
        registry.incrementCounter("test1", 2.0)
        registry.incrementCounter("test2")
        registry.incrementCounter("test2", 4.0)
        assertThat(registry, notNullValue())
        assertThat(registry.registryOption.isSome(), equalTo(true))
    }

    @Test
    fun shouldCreateCounter2() {
        val registry = MetricsRegistry(meterRegistry)
        registry.incrementCounter("test1")
        registry.incrementCounter("test1", 2.0)
        registry.incrementCounter("test2", "tag", "value")
        registry.incrementCounter("test2", 4.0, "tag", "value")
        assertThat(registry, notNullValue())
        assertThat(registry.registryOption.isSome(), equalTo(true))
    }

    @Test
    fun shouldCreateGauge1() {
        val registry = MetricsRegistry(meterRegistry)
        assertThat(registry, notNullValue())
        assertThat(registry.registryOption.isSome(), equalTo(true))
        val gauge = registry.gauge(AtomicInteger(0), "gauge")
        assertThat(gauge, notNullValue())
        assertThat(gauge.isSome(), equalTo(true))
        gauge.map { it.incrementAndGet() }
        assertThat(gauge.getOrElse { null }!!.get(), equalTo(1))
    }

    @Test
    fun shouldCreateGauge2() {
        val registry = MetricsRegistry(meterRegistry)
        assertThat(registry, notNullValue())
        assertThat(registry.registryOption.isSome(), equalTo(true))
        val gauge = registry.gauge(AtomicInteger(0), "gauge", "tag", "value")
        assertThat(gauge, notNullValue())
        assertThat(gauge.isSome(), equalTo(true))
        gauge.map { it.incrementAndGet() }
        assertThat(gauge.getOrElse { null }!!.get(), equalTo(1))
    }

    @Test
    fun shouldCreateGaugeFromObject1() {
        val registry = MetricsRegistry(meterRegistry)
        assertThat(registry, notNullValue())
        assertThat(registry.registryOption.isSome(), equalTo(true))

        val data = TestData("123")

        val gauge = registry.gaugeObject(data, "gauge", {
            it.data.toDouble()
        })

        assertThat(gauge, notNullValue())
        assertThat(gauge.isSome(), equalTo(true))
        assertThat(gauge.getOrElse { null }!!.value(), equalTo("123".toDouble()))
        data.data = "234"

        assertThat(gauge.getOrElse { null }!!.value(), equalTo("234".toDouble()))
    }

    @Test
    fun shouldCreateGaugeFromObject2() {
        val registry = MetricsRegistry(meterRegistry)
        assertThat(registry, notNullValue())
        assertThat(registry.registryOption.isSome(), equalTo(true))

        val data = TestData("123")

        val gauge = registry.gaugeObject(data, "gauge", {
            it.data.toDouble()
        }, "abc", "def")

        assertThat(gauge, notNullValue())
        assertThat(gauge.isSome(), equalTo(true))
        assertThat(gauge.getOrElse { null }!!.value(), equalTo("123".toDouble()))
        data.data = "234"

        assertThat(gauge.getOrElse { null }!!.value(), equalTo("234".toDouble()))
    }

    @Test
    fun shouldReturnEmptyExecutor() {
        val registry = MetricsRegistry(SimpleMeterRegistry())
        assertThat(
            registry.measureExecutorService(fromNullable<ExecutorService>(null), "abc").isSome(),
            equalTo(false)
        )
        assertThat(
            MetricsRegistry().measureExecutorService(fromNullable(null), "abc").isSome(),
            equalTo(false)
        )
        assertThat(
            MetricsRegistry().measureExecutorService(fromNullable<ExecutorService>(null), "abc")
                .isSome(),
            equalTo(false)
        )
    }

    @Test
    fun shouldReturnExecutor() {
        val registry = MetricsRegistry(SimpleMeterRegistry())
        val executorService = Executors.newCachedThreadPool()
        val monitoredExecutorServiceOption =
            registry.measureExecutorService(fromNullable(executorService), "abc")
        assertThat(
            monitoredExecutorServiceOption.isSome(),
            equalTo(true)
        )
        assertThat(monitoredExecutorServiceOption.toList(), Matchers.hasSize(1))
        assertThat(
            monitoredExecutorServiceOption.toList()[0],
            not(sameInstance(executorService))
        )
        val unMonitoredExecutorServiceOption =
            MetricsRegistry().measureExecutorService(fromNullable(executorService), "abc")
        assertThat(unMonitoredExecutorServiceOption.isSome(), equalTo(true))
        assertThat(unMonitoredExecutorServiceOption.toList(), hasSize(1))
        assertThat(unMonitoredExecutorServiceOption.toList()[0], sameInstance(executorService))
    }
}
