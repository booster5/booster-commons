package io.gitlab.booster.commons.circuit.breaker

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class CircuitBreakerSettingBuilderTest {

    @Test
    fun failureRateThreshold() {
        val setting = CircuitBreakerSettingBuilder()
            .failureRateThreshold(100)
            .build()
        assertThat(setting.failureRateThreshold, equalTo(100))
    }

    @Test
    fun slowCallRateThreshold() {
        val setting = CircuitBreakerSettingBuilder()
            .slowCallRateThreshold(10)
            .build()
        assertThat(setting.slowCallRateThreshold, equalTo(10))
    }

    @Test
    fun slowCallDurationThreshold() {
        val setting = CircuitBreakerSettingBuilder()
            .slowCallDurationThreshold(20)
            .build()
        assertThat(setting.slowCallDurationThreshold, equalTo(20))
    }

    @Test
    fun permittedNumberOfCallsInHalfOpenState() {
        val setting = CircuitBreakerSettingBuilder()
            .permittedNumberOfCallsInHalfOpenState(5)
            .build()
        assertThat(setting.permittedNumberOfCallsInHalfOpenState, equalTo(5))
    }

    @Test
    fun maxWaitDurationInHalfOpenState() {
        val setting = CircuitBreakerSettingBuilder()
            .maxWaitDurationInHalfOpenState(10)
            .build()
        assertThat(setting.maxWaitDurationInHalfOpenState, equalTo(10))
    }

    @Test
    fun slidingWindowSize() {
        val setting = CircuitBreakerSettingBuilder()
            .slidingWindowSize(10)
            .build()
        assertThat(setting.slidingWindowSize, equalTo(10))
    }

    @Test
    fun minimumNumberOfCalls() {
        val setting = CircuitBreakerSettingBuilder()
            .minimumNumberOfCalls(1)
            .build()
        assertThat(setting.minimumNumberOfCalls, equalTo(1))
    }

    @Test
    fun waitDurationInOpenState() {
        val setting = CircuitBreakerSettingBuilder()
            .waitDurationInOpenState(1000)
            .build()
        assertThat(setting.waitDurationInOpenState, equalTo(1000))
    }

    @Test
    fun isAutomaticTransitionFromOpenToHalfOpenEnabled() {
        val setting = CircuitBreakerSettingBuilder()
            .isAutomaticTransitionFromOpenToHalfOpenEnabled(true)
            .build()
        assertThat(setting.isAutomaticTransitionFromOpenToHalfOpenEnabled, equalTo(true))
    }

    @Test
    fun slidingWindowType() {
        val setting = CircuitBreakerSettingBuilder()
            .slidingWindowType(CircuitBreakerSetting.SlidingWindowType.TIME_BASED)
            .build()
        assertThat(setting.slidingWindowType, equalTo(CircuitBreakerSetting.SlidingWindowType.TIME_BASED))
    }

    @Test
    fun `should pass default build`() {
        val setting = CircuitBreakerSettingBuilder().build()
        assertThat(setting, notNullValue())

        assertThat(
            setting.failureRateThreshold,
            equalTo(CircuitBreakerSetting.DEFAULT_FAILURE_THRESHOLD)
        )
        assertThat(
            setting.slowCallRateThreshold,
            equalTo(CircuitBreakerSetting.DEFAULT_SLOW_CALL_THRESHOLD)
        )
        assertThat(
            setting.slowCallDurationThreshold,
            equalTo(CircuitBreakerSetting.DEFAULT_SLOW_CALL_DURATION_THRESHOLD)
        )
        assertThat(
            setting.slidingWindowSize,
            equalTo(CircuitBreakerSetting.DEFAULT_SLIDING_WINDOW_SIZE)
        )
        assertThat(
            setting.slidingWindowType,
            equalTo(CircuitBreakerSetting.SlidingWindowType.COUNT_BASED)
        )
        assertThat(
            setting.isAutomaticTransitionFromOpenToHalfOpenEnabled,
            equalTo(false)
        )
        assertThat(
            setting.permittedNumberOfCallsInHalfOpenState,
            equalTo(CircuitBreakerSetting.DEFAULT_PERMITTED_NUMBER_OF_CALLS_IN_HALF_OPEN_STATE)
        )
        assertThat(
            setting.waitDurationInOpenState,
            equalTo(CircuitBreakerSetting.DEFAULT_WAIT_DURATION_IN_OPEN_STATE)
        )
        assertThat(
            setting.minimumNumberOfCalls,
            equalTo(CircuitBreakerSetting.DEFAULT_MINIMUM_NUMBER_OF_CALLS)
        )
        assertThat(
            setting.maxWaitDurationInHalfOpenState,
            equalTo(CircuitBreakerSetting.DEFAULT_MAX_WAIT_DURATION_IN_HALF_OPEN_STATE)
        )
    }
}
