package io.gitlab.booster.commons.retry

import io.gitlab.booster.commons.metrics.MetricsRegistry
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

internal class RetryConfigTest {
    @Test
    fun shouldCreateConfig() {
        assertThat(RetryConfig(), notNullValue())
        assertThat(RetryConfig(mapOf()), notNullValue())
        assertThat(RetryConfig(mapOf(Pair("test", RetrySetting()))), notNullValue())
    }

    @Test
    fun shouldNotCreateRetry() {
        assertThat(RetryConfig().tryGet("test").isSome(), equalTo(false))
        assertThat(RetryConfig(mapOf()).tryGet("test").isSome(), equalTo(false))
        assertThat(
            RetryConfig(mapOf(Pair("abc", RetrySetting()))).tryGet("test").isSome(),
            equalTo(false)
        )
    }

    @Test
    fun shouldCreateRetry() {
        val setting = RetrySetting()
        setting.maxAttempts = 1
        assertThat(
            RetryConfig(mapOf(Pair("test", setting))).tryGet("test").isSome(),
            equalTo(true)
        )
    }

    @Test
    fun `should allow null setting`() {
        val config = RetryConfig()
        config.setSettings(null)

        assertThat(
            config.tryGet("name").isSome(),
            equalTo(false)
        )
    }

    @Test
    fun `should allow non-null setting`() {
        val setting = RetrySetting()
        setting.maxAttempts = 1

        val config = RetryConfig()
        config.setSettings(mapOf(Pair("test", setting)))

        assertThat(
            config.tryGet("test").isSome(),
            equalTo(true)
        )
    }

    @Test
    fun shouldHandleRegistry() {
        val setting = RetrySetting()
        setting.maxAttempts = 1
        val config = RetryConfig(mapOf(Pair("test", setting)))
        config.setMetricsRegistry(null)
        assertThat(
            config.tryGet("test").isSome(), equalTo(true)
        )
        assertThat(
            config.tryGet("abc").isSome(), equalTo(false)
        )
        config.setMetricsRegistry(MetricsRegistry())
        assertThat(
            config.tryGet("test").isSome(), equalTo(true)
        )
        assertThat(
            config.tryGet("abc").isSome(), equalTo(false)
        )
        config.setMetricsRegistry(MetricsRegistry(SimpleMeterRegistry()))
        assertThat(
            config.tryGet("test").isSome(), equalTo(true)
        )
        assertThat(
            config.tryGet("abc").isSome(), equalTo(false)
        )
    }

    @Test
    fun `should allow customization`() {
        val setting = RetrySetting()
        setting.maxAttempts = 1
        val config = RetryConfig(mapOf(Pair("test", setting)))
        config.setMetricsRegistry(null)
        assertThat(
            config.tryGet("test").isSome(), equalTo(true)
        )

        val retryOption = config.customize<Any>("test") {
            it.maxAttempts(2)
                .retryExceptions(IllegalStateException::class.java)
                .failAfterMaxAttempts(true)
        }
        assertThat(retryOption, notNullValue())
        assertThat(retryOption.isSome(), equalTo(true))
        val retry = retryOption.getOrNull()!!
        assertThat(
            retry.retryConfig.maxAttempts,
            equalTo(2)
        )
    }
}
