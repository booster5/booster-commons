package io.gitlab.booster.commons.retry

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class RetrySettingBuilderTest {

    @Test
    fun backOffPolicy() {
        val setting = RetrySettingBuilder()
            .backOffPolicy(RetrySetting.BackOffPolicy.EXPONENTIAL)
            .build()
        assertThat(setting.backOffPolicy, equalTo(RetrySetting.BackOffPolicy.EXPONENTIAL))
    }

    @Test
    fun maxAttempts() {
        val setting = RetrySettingBuilder()
            .maxAttempts(10)
            .build()
        assertThat(setting.maxAttempts, equalTo(10))
    }

    @Test
    fun initialBackOffMillis() {
        val setting = RetrySettingBuilder()
            .initialBackOffMillis(1000)
            .build()
        assertThat(setting.initialBackOffMillis, equalTo(1000))
    }

    @Test
    fun build() {
        val setting = RetrySettingBuilder().build()
        assertThat(
            setting.maxAttempts,
            equalTo(1)
        )
        assertThat(
            setting.backOffPolicy,
            equalTo(RetrySetting.BackOffPolicy.LINEAR)
        )
        assertThat(
            setting.initialBackOffMillis,
            equalTo(RetrySetting.DEFAULT_INITIAL_BACKOFF_MILLIS)
        )
    }
}
