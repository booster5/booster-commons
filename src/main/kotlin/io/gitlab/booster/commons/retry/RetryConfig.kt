package io.gitlab.booster.commons.retry

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable
import io.gitlab.booster.commons.cache.GenericKeyedObjectCache
import io.gitlab.booster.commons.cache.KeyedCacheObjectFactory
import io.gitlab.booster.commons.cache.KeyedObjectCache
import io.gitlab.booster.commons.metrics.MetricsRegistry
import io.github.resilience4j.retry.Retry
import io.github.resilience4j.retry.RetryRegistry
import org.slf4j.LoggerFactory
import java.util.function.Consumer

/**
 * Provides a central repository for [Retry] management.
 */
class RetryConfig @JvmOverloads constructor(settings: Map<String, RetrySetting>? = null) :
    KeyedCacheObjectFactory<String, Retry>, KeyedObjectCache<String, Retry> {

    private var settings: Map<String, RetrySetting>

    private var registry: MetricsRegistry? = null

    private var pool: GenericKeyedObjectCache<String, Retry>

    /**
     * Constructor with default retry settings.
     * @param settings map of [RetrySetting] identified by name
     */
    init {
        this.settings = settings ?: HashMap()
        this.pool = GenericKeyedObjectCache(this)
    }

    override fun create(key: String): Retry? {
        log.debug("booster-commons - cache contains [{}] entry: {}", key, settings.containsKey(key))
        return if (settings.containsKey(key))
            settings[key]!!.buildRetry(key, registry).getOrNull()
        else null
    }

    override fun get(key: String): Retry? = this.pool.get(key)

    fun setSettings(settings: Map<String, RetrySetting>?) {
        this.settings = settings ?: mapOf()
        this.pool = GenericKeyedObjectCache(this)
    }

    fun getSettings() = this.settings

    fun setMetricsRegistry(registry: MetricsRegistry?) {
        this.registry = registry ?: MetricsRegistry()
    }

    companion object {
        private val log = LoggerFactory.getLogger(RetryConfig::class.java)

        fun createRetry(
            name: String,
            config: io.github.resilience4j.retry.RetryConfig,
            metricsRegistry: MetricsRegistry?): Option<Retry> {
            val registry = RetryRegistry.of(config)
            metricsRegistry?.bind(registry)
            return fromNullable(registry.retry(name, config))
        }
    }

    override fun getKeys(): Set<String> = this.pool.getKeys()

    /**
     * Creates a new [Retry] based on an existing [RetrySetting]
     * @param name name of the [RetrySetting] to use as a base for new [Retry]
     * @param customizer consumer that takes a
     *                   [io.github.resilience4j.retry.RetryConfig.Builder] and
     *                   customizes the [Retry] to be created
     * @return [Option] of [Retry]
     */
    fun <T> customize(
        name: String,
        customizer: Consumer<io.github.resilience4j.retry.RetryConfig.Builder<T>>
    ): Option<Retry> {
        val config = this.get(name)?.retryConfig
        if (config == null) {
            return fromNullable(null)
        } else {
            val builder: io.github.resilience4j.retry.RetryConfig.Builder<T> =
                io.github.resilience4j.retry.RetryConfig.from(config)
            customizer.accept(builder)
            val newConfig = builder.build()
            val retryRegistry = RetryRegistry.of(newConfig)
            this.registry?.bind(retryRegistry)
            return fromNullable(retryRegistry.retry(name, newConfig))
        }
    }
}
