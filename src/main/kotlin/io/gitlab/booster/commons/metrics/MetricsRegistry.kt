package io.gitlab.booster.commons.metrics

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable
import arrow.core.getOrElse
import arrow.core.orElse
import arrow.core.recover
import com.google.common.base.Preconditions
import io.github.resilience4j.micrometer.tagged.TaggedRetryMetrics
import io.github.resilience4j.retry.RetryRegistry
import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.Gauge
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Tags
import io.micrometer.core.instrument.Timer
import io.micrometer.core.instrument.binder.jvm.ExecutorServiceMetrics
import io.micrometer.core.instrument.internal.TimedExecutorService
import lombok.Getter
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import java.util.concurrent.ExecutorService
import java.util.function.ToDoubleFunction
import java.util.stream.Stream

/**
 * Micrometer registry wrapper that allows one to
 * insert metric recording code without Micrometer support.
 */
@Getter
class MetricsRegistry @JvmOverloads constructor(
    registry: MeterRegistry? = null
) {
    private val registry: Option<MeterRegistry>

    /**
     * Constructor with provided [MeterRegistry]
     * @param registry [MeterRegistry], if null behaves the same as noop constructor.
     */
    init {
        this.registry = fromNullable(registry)
    }

    val registryOption: Option<MeterRegistry>
        get() {
            return this.registry
        }

    /**
     * Start a timer sample.
     * @return Optional sample
     */
    fun startSample(): Option<Timer.Sample> {
        return registry.map { Timer.start(it) }
    }

    /**
     * Stop a timer sample and record the time.
     * @param sampleTimer sample to stop
     * @param name name of the timer
     * @param tags optional tags for the timer.
     */
    @Suppress("SpreadOperator")
    fun endSample(sampleTimer: Option<Timer.Sample>, name: String, vararg tags: String) {
        registry.map { reg: MeterRegistry ->
            sampleTimer.map { sample: Timer.Sample ->
                sample.stop(
                    reg.timer(
                        name,
                        *tags
                    )
                )
            }
        }
    }

    /**
     * Increase counter by 1
     * @param name name of the counter to increase
     * @param tags tags for the counter
     */
    @Suppress("SpreadOperator")
    fun incrementCounter(name: String, vararg tags: String) {
        registry.map { reg: MeterRegistry -> reg.counter(name, *tags) }
            .map { counter: Counter ->
                counter.increment()
                counter
            }
    }

    /**
     * Increase counter by specified amount.
     * @param name name of counter to increase
     * @param increment amount to increase
     * @param tags tags for the counter
     */
    @Suppress("SpreadOperator")
    fun incrementCounter(name: String, increment: Double, vararg tags: String) {
        registry.map { reg: MeterRegistry -> reg.counter(name, *tags) }
            .map { counter: Counter ->
                counter.increment(increment)
                counter
            }
    }

    /**
     * Creates gauge from object.
     */
    @Suppress("SpreadOperator")
    fun <T> gaugeObject(obj: T, name: String, func: ToDoubleFunction<T>, vararg  tags: String): Option<Gauge> =
        registry.map {
            return@map Gauge.builder(name, obj, func)
                .tags(Tags.of(*tags))
                .strongReference(true)
                .register(it)
        }

    /**
     * Set value for gauge.
     * @param state initial state of the gauge value.
     * @param name name of the gauge.
     * @param tags tags for the gauge
     * @param <T> Type of gauge value.
     * @return Optional value of the gauge state.
    </T> */
    @Suppress("SpreadOperator")
    fun <T : Number> gauge(state: T, name: String, vararg tags: String): Option<T> {
        return registry.map {
            fromNullable(
                it.gauge(
                    name,
                    Tags.of(*tags),
                    state
                )
            )
        }.getOrElse { fromNullable(null) }
    }

    /**
     * Monitor thread pool usage
     * @param executorService [ExecutorService] to be monitored
     * @param name value to use for name tag
     * @return a monitored [ExecutorService]
     */
    fun measureExecutorService(executorService: Option<ExecutorService>, name: String): Option<ExecutorService> {
        Preconditions.checkArgument(StringUtils.isNotBlank(name), "name cannot be blank")

        return registry.flatMap { reg: MeterRegistry? ->
                executorService.map { executor: ExecutorService? ->
                    log.debug("booster-task - attempting to measure thread pool: [{}]", name)
                    // if already monitored, return directly.
                    if (executor is TimedExecutorService) {
                        log.debug("booster-task - thread pool [{}] already monitored", name)
                        return@map executor
                    }
                    // otherwise, monitor it.
                    log.debug("booster-task - monitoring thread pool [{}]", name)
                    ExecutorServiceMetrics.monitor(reg!!, executor!!, name)
                }
            }.recover { executorService.bind() }
    }

    fun bind(retryRegistry: RetryRegistry) {
        this.registry.map {
            TaggedRetryMetrics.ofRetryRegistry(retryRegistry).bindTo(it)
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(MetricsRegistry::class.java)

        /**
         * Trace ID tag.
         */
        const val TRACE_ID = "traceId"
    }
}
