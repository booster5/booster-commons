package io.gitlab.booster.commons.cache

interface KeyedCacheObjectFactory<K, V> {

    fun create(key: K): V?
}
